To test the server you can run the following curl commands 

$  # retrieve all spells

$  curl -X GET -sS http://localhost:1234/getSpells 
 


$  # retrieve only the Silencio spell

$  curl -X GET -sS http://localhost:1234/getSpell?name=Silencio 

{"name":"Silencio","type":"spell","effect":"Silences victim"}



$  # get a spell that does not exist

$  curl -X GET -sS http://localhost:1234/getSpell?name=xyzxyz

{"msg":"spell not found: xyzxyz"}



$  # add a new spell

$  curl --header "Content-Type: application/json" --request POST -sS --data '{ "name":"Zap","type":"spell","effect":"Shocks victim." }' http://localhost:1234/addSpell

{"msg":"spell added."}



$  # get the spell that was just added

$  curl -X GET -sS http://localhost:1234/getSpell?name=Zap

{"name":"Zap","type":"spell","effect":"Shocks victim."}



$  # try to add the same spell

$  curl --header "Content-Type: application/json" --request POST -sS --data '{ "name":"Zap","type":"spell","effect":"Shocks victim." }' http://localhost:1234/addSpell

{"msg":"error - spell already exists!"}
